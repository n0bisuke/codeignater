<?php
Class Bbs_model extends CI_Model{
 	function Bbs_model(){
 		parent::__construct();
 		$this->load->database();
 	}

 	public function get_all(){
		if ($this->db->conn_id === FALSE){
			return "db_error"; //DBに接続できない場合はエラーを返す
		}else{
			$this->db->select('id, title, comment');
			$this->db->from('ambitious_test_bbs');
			$this->db->order_by('ambitious_test_bbs.id', 'desc');
			$query = $this->db->get();
			return $query;
		}
	}
}