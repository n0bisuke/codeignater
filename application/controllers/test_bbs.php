<?php
	class Test_bbs extends CI_Controller{
		function Test_bbs(){
			parent::__construct();
			$this->load->helper('url'); //リダイレクトで利用
			$this->load->helper('form'); //formヘルパーの利用
			$this->load->database(); //DB接続に利用
			$this->load->model('bbs_model');
			$this->load->library('session'); //session利用
		}

		function show(){//掲示板表示
			$this->is_login();
			$data['query'] = $this->bbs_model->get_all(); //全部取得
			$this->load->view("bbs_show", $data);

			var_dump($this->session->userdata('USER_STATUS'));
			var_dump($this->session->userdata('USERNAME'));
		}

		function create(){//投稿画面
			$this->is_login();
			$this->load->view("bbs_form");//ビューレンダリング
			if($_POST){
				$input_data = array(
		    		'title' => $this->input->post('title'),
		    		'comment' => $this->input->post('comment')
		    	);

				if($this->input->post('id')){//編集の場合
		    		$this->db->where('id', $this->input->post('id'));
		    		$this->db->update('ambitious_test_bbs', $input_data);
				}else{//新規投稿の場合
					$this->db->insert('ambitious_test_bbs', $input_data);
				}
				
				redirect('test_bbs/show');
			}
		}

		function edit($id=''){//編集フォーム
			$this->is_login();
			$id = (int)$id;
			$limit = 1;
			$response = $this->db->get_where('ambitious_test_bbs', array('id' => $id), $limit);
			$data['query'] = $response->result()[0];
			$this->load->view("bbs_form",$data);//ビューレンダリング createと共通利用
		}

		function delete($id=''){//削除
			$this->is_login();
			$id = (int)$id;
			$this->db->where("id",$id);
			$this->db->delete('ambitious_test_bbs');
			redirect('test_bbs/show');
		}

		/**
		認証系処理
		**/
		//ログインチェック
		function is_login(){
			$USER_STATUS = $this->session->userdata('USER_STATUS');
			if($USER_STATUS != 'LOGIN') {
				redirect('test_bbs/login', 'location');
			}
		}
		
		function logout() {
    		$this->session->sess_destroy();
    		redirect('test_bbs/login', 'location');
  		}

  		//login
  		public function login(){
  			//ログイン済みの場合はshowへ
  			$USER_STATUS = $this->session->userdata('USER_STATUS');
			if($USER_STATUS == 'LOGIN') {
				redirect('test_bbs/show', 'location');
			}
			
  			$account =  $this->input->post('account', true);
  			$password = $this->input->post('password', true);

  			$limit = 1;
			$response = $this->db->get_where('ambitious_test_user', 
				array(
					'name' => $account,
					'password' => $password
				), $limit);

			var_dump($response->result());

		    if ( ($account === 'boo')&&($password === 'foo')){
				$this->session->set_userdata('USERNAME', $account); //ログインユーザー名
				$this->session->set_userdata('USER_STATUS', 'LOGIN'); //ログインFLAG
				redirect('test_bbs/show', 'location'); //ログイン成功後のリダイレクト先
		    }else{
		    	$this->load->view('bbs_login');
		    }
		}

		//ユーザー登録
		function sign_up(){
			$account =  $this->input->post('account', true);
  			$password = $this->input->post('password', true);
  			$passcheck = $this->input->post('passcheck', true);

  			if($password === $passcheck){//パスワード確認があっているかどうか
				$input_data = array(
		    		'name' => $account,
		    		'password' => $password
		    	);
		    	$this->db->insert('ambitious_test_user', $input_data);
		    	redirect('test_bbs/login', 'location'); 
		    }else{
		    	$this->load->view('bbs_login');
		    }
		}
	}